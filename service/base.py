from sqlalchemy.orm import sessionmaker
from db.database import engine
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

class PostgresBase:

    def __init__(self, engine: create_engine = engine):
        self.engine = engine
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.commit()
        self.session.close()
