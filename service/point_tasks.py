from models.schema import PointTasks
from datetime import datetime
from service.base import PostgresBase
from typing import List



class PointTasksService(PostgresBase):

    def __init__(self):
        super().__init__()
        self.query = self.session.query(PointTasks)

    def get_task_by_id(self, _id):
        task = self.query.filter_by(id=_id).first()
        return task

    def add_many_task(self, tasks: List[PointTasks]):
        self.session.add_all(tasks)
        self.session.commit()
        return f'заданий добавлено {len(tasks)}'

    def get_batch_tasks(self, size: int, modified: datetime):
        tasks = self.query.filter(PointTasks.modified >= modified)
        return tasks.limit(size)
