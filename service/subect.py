from models.schema import Subject, Country
from datetime import datetime
from service.base import PostgresBase
from typing import List


class SubjectService(PostgresBase):

    def __init__(self):
        super().__init__()
        self.query = self.session.query(Subject)

    def get_subjects_by_country(self, country_name: str):
        subjects = self.query.join(Country).filter(Country.name == country_name).all()
        return subjects
