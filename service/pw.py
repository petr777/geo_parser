
class Build:

    def __init__(self, playwright):
        self.playwright = playwright
        self.browser = playwright.firefox.launch(
            headless=False, slow_mo=50
        )
        self.context = self.browser.new_context()
        self.page = self.context.new_page()

    def get_content(self, url):
        self.page.goto(url)
        return self.page.content()

