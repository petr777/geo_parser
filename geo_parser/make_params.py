def get_params(lat, lon, num_page, page_size):
    params = {
        'key': 'ruxlih0718',
        'point': f'{lon},{lat}',
        'page': num_page,
        'page_size': page_size,
        'radius': 2000,
        'type': 'building',
        'fields': ','.join([
            'items.point',
            'items.address',
            'items.adm_div',
            'items.full_address_name',
            'items.geometry.centroid',
            'items.geometry.hover',
            'items.geometry.selection',
            'items.floors',
            # --- Список полей с дополнительной информацией о месте --- #
            'items.rubrics',  # — категории компании;
            'items.org',  # — организация, к которой относится филиал;
            'items.contact_groups',  # — контакты компании;
            'items.schedule',  # — расписание работы компании;
            'items.access_comment',  # — локализованное название для типа доступа;
            'items.access',  # — тип доступа для парковки;
            'items.capacity',  # — вместимость парковки;
            'items.description',  # — описание геообъекта;
            'items.external_content',  # — дополнительные данные компании, такие как буклеты и фотографии;
            'items.flags',  # — список признаков объекта;

            'items.is_paid',  # — является ли парковка платной;
            'items.is_incentive',  # — является ли парковка перехватывающей;
            'items.purpose',  # — назначение парковки;
            'items.level_count',  # — количество уровней парковки;
            'items.links',  # — связанные объекты;
            'items.links.database_entrances.apartments_info'
            'items.name_ex',  # — составные части наименования объекта;
            'items.reviews',  # — отзывы об объекте;
            'items.statistics',  # — cводная информация о геообъекте;
            'items.employees_org_count',  # — численность сотрудников организации;
            'items.itin',  # — индивидуальный номер налогоплательщика;
            'items.trade_license',  # — лицензия филиала;

            # # --- Список служебных полей --- #
            'context_rubrics',  # — массив контекстных категорий;
            'dym',  # — блок «Возможно, вы имели ввиду»;
            'filters',  # — фильтры для дополнительного поиска;
            'hash',  # — базовый хеш;
            'items.ads.options',  # — рекламные опции;
            'items.attribute_groups',  # — дополнительные атрибуты компании;
            'items.context',  # — динамическая информация;
            'items.dates.deleted_at',  # — дата удаления организации из базы в формате ISO 8601;
            'items.dates.updated_at',  # — дата последнего изменения информации об организации в формате ISO 8601;
            'items.dates',  # — время внесения информации о компании в БД;
            'items.geometry.style',  # — идентификатор стиля для отображения;
            'items.group',  # — связанные в объединённую карточку объекты;
            'items.metarubrics',  # — метарубрики для выдачи организаций в гибридном объекте;
            'items.delivery',  # — есть доставка;
            'items.covid_safety',  # — признак covid безопасности;
            'items.has_goods',  # — загружен список товаров фирмы;
            'items.has_payments',  # — есть возможность оплатить услуги фирмы онлайн;
            'items.has_pinned_goods',  # — у фирмы включен блок «Закрепленные товары»;
            'items.has_realty',  # — есть недвижимость на продаже;
            'items.has_discount',  # — есть скидки;
            'items.has_exchange',  # — признак наличия курсов валют у филиала;
            'items.is_main_in_group',  # — признак того, что это главный объект в группе объектов гибрида;
            'items.city_alias',  # — алиас города, в котором находится объект;
            'items.alias',  # — транслитерированное название объекта;
            'items.caption',  # — название объекта;
            'items.is_promoted',  # — фирма участвует в промо-акции Чека;
            'items.routes',  # — маршруты транспорта, проходящие через станцию или остановку;
            'items.directions',  # — направления маршрута;
            'items.barrier',  # — тип заграждения;
            'items.is_routing_available',  # — флаг, возможен ли проезд до объекта;
            'items.entrance_display_name',

            # — показать номер входа на станцию метро, если объект является входом (station_entrance);
            'items.locale',  # — текущая локаль для региона;
            'items.reg_bc_url',  # — URL для регистрации бизнес-коннекшна просмотра профиля;
            'items.region_id',  # — уникальный идентификатор проекта;
            'items.segment_id',  # — уникальный идентификатор сегмента;
            'items.stat',  # — данные для формирования сообщений статистики;
            'items.stop_factors',  # — набор блокирующих атрибутов, соответствующих запросу;
            'items.has_apartments_info',  # — признак наличия информации о квартирах в здании;
            'items.timezone',  # — часовой пояс в формате POSIX;
            'items.timezone_offset',  # — смещение таймзоны в минутах относительно UTC0;
            'items.comment',  # — комментарий ко входу;
            'items.station_id',  # — уникальный идентификатор остановки, к которой относится остановочная платформа;
            'items.platforms',  # — остановочные платформы остановки;
            'items.sources',  # — идентификатор источника данных об объекте;
            'items.structure_info',  # — данные о количестве квартир и материале здания;
            # 'items.structure_info.material', # — данные о материале здания;
            # 'items.structure_info.apartments_count', # — данные о количестве квартир;
            # 'items.structure_info.porch_count', # — данные о количестве подъездов;
            'items.route_logo',  # — иконка метро;
            'items.reply_rate',  # — время ответа организации;
            'items.order_with_cart',  # — свойства заказа у организации без доставки, но с корзиной;
            'items.is_deleted',  # — признак удаленного объекта;
            'items.search_attributes',  # — параметры результата поиска для найденного объекта;
            'request_type',  # — тип поискового запроса;
            'search_attributes',  # — информация о произведённом поиске;
            'widgets',  # — виджеты;
            'broadcast',  # — информация для отправки броадкаст сообщений в компании.
        ])
    }
    return params
