from service.point_tasks import PointTasksService
from service.pw import Build
from datetime import datetime
from geoalchemy2.shape import to_shape
from make_params import get_params
from urllib.parse import urlencode
from playwright.sync_api import sync_playwright





def run(playwright, tasks):
    build = Build(playwright)
    for task in tasks:
        shape = to_shape(task.point)
        lat, lon = shape.y, shape.x
        params = get_params(lat=lat, lon=lon, num_page=1, page_size=200)
        url = 'http://catalog.api.2gis.ru/2.0/geo/search?' + urlencode(params)
        content = build.get_content(url)


point_task_service = PointTasksService()
tasks = point_task_service.get_batch_tasks(
    size=50,
    modified=datetime.min
)

with sync_playwright() as playwright:
    run(playwright, tasks)


