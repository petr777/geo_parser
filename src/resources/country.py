from models.country import CountryModel
from resources.base import Base
from dependencies.kz import all_regions as kz_region
from dependencies.ru import all_regions as ru_region
from dependencies.uz import all_regions as uz_region


class Country(Base):

    model = CountryModel

    @staticmethod
    def get_region_names(country_name):
        data = {
            'Россия': ru_region,
            'Казахстан': kz_region,
            'Узбекистан': uz_region,
        }
        return data.get(country_name)
