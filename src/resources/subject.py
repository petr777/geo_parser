from models.country import SubjectModel
from resources.base import Base


class Subject(Base):

    model = SubjectModel

    def get_subject_by_country_name(self, country_name: str):
        return self.session.query(self.model).filter(
            self.model.country.has(name=country_name)
        ).all()
