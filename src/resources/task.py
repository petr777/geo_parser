from models.country import TaskModel
from resources.base import Base
from datetime import datetime
from sqlalchemy import and_
from sqlalchemy.dialects.postgresql import insert
from typing import List
from core.config import settings
from resources.state import JsonFileStorage, State
from resources.subject import Subject


class Task(Base):

    model = TaskModel
    subject_service = Subject()
    storage = JsonFileStorage(settings.LocalStorage)
    state = State(storage)

    def add_many_task(self, tasks: List[dict], index_elements: List[str]):
        for task in tasks:
            insert_table = insert(self.model).values(task)
            insert_table_sql = insert_table.on_conflict_do_nothing(
                index_elements=index_elements
            )
            self.session.execute(insert_table_sql)
        self.session.commit()
        return len(tasks)

    def get_task_by_subject_name(self, subject_name: str):
        return self.session.query(self.model).filter(
            self.model.subject.has(name=subject_name)
        ).all()

    def get_batch_gt_visited(self, limit: int, subject_id: str, visited_state: datetime):

        return self.session.query(self.model).filter(
            and_(self.model.subject.has(id=subject_id),
                 self.model.visited > visited_state)
        ).limit(limit).all()

    def get_batch(self, subject_id: str = None, limit_batch: int = 100):
        state = self.state
        subject = self.subject_service.get_by_id(subject_id)

        if not state.get_state(subject.name):
            state.set_state(subject.name, settings.initial_state)

        current_state = datetime.fromisoformat(
            state.get_state(subject.name)
        )
        batch_task = self.get_batch_gt_visited(
            limit=limit_batch,
            subject_id=subject_id,
            visited_state=current_state
        )
        return batch_task
