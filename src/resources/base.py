from sqlalchemy.orm import Session
from db.postgis import SessionLocal
from abc import ABC
from sqlalchemy.dialects.postgresql import insert


class Base(ABC):

    model = None

    def __init__(self, session: Session = SessionLocal()):
        self.session = session

    def add(self, data: dict, index_elements: list = None):
        insert_table = insert(self.model).values(data)
        insert_table_sql = insert_table.on_conflict_do_nothing(index_elements=index_elements)
        result = self.session.execute(insert_table_sql)
        self.session.commit()
        return result

    def get_all(self):
        return self.session.query(self.model).all()

    def get_by_id(self, _id):
        return self.session.query(self.model).get(_id)

    def get_by_name(self, name):
        return self.session.query(self.model).filter(self.model.name == name).first()
