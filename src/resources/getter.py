from playwright.sync_api import sync_playwright
import json
from core.constants import RADIUS, PAGE_SIZE, API_KEY, ALL_FIELDS


class GetterGeometry:

    def __init__(self, playwright: sync_playwright = sync_playwright()):
        playwright = playwright.start()
        self.browser = playwright.chromium.launch(
            headless=False, slow_mo=50
        )
        self.context = self.browser.new_context()
        self.page = self.context.new_page()

    def params_maker(self, lat: float, lon: float, page: int = 1, **kwargs):
        params = {
            'key': API_KEY,
            'point': f'{lat},{lon}',
            'page': page,
            'page_size': PAGE_SIZE,
            'radius': RADIUS,
            'type': 'building',
            'fields': ','.join(ALL_FIELDS),
            'locale': 'ru_RU',
        }
        return params

    def goto(self, url):
        self.page.goto(url)

    def get_json(self):
        html = self.page.query_selector('pre')
        html = html.inner_html()
        return json.loads(html)

    def close(self):
        self.page.close()
        self.browser.close()
