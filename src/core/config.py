import os
from pydantic import BaseSettings
from os.path import join
from datetime import datetime
import jinja2

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


class Base(BaseSettings):

    class Config:
        env_file = join(BASE_DIR, '.env')
        env_file_encoding = 'utf-8'
        arbitrary_types_allowed = True


class PostgresSettings(Base):

    host: str = os.getenv('POSTGRES_HOST', 'localhost')
    port: str = os.getenv('POSTGRES_PORT', 5432)
    name: str = os.getenv('POSTGRES_DB', 'geo')
    user: str = os.getenv('POSTGRES_USER', 'admin')
    password: str = os.getenv('POSTGRES_USER', 'admin')

    class Config:
        env_prefix = 'postgres_'


class Settings(Base):

    db: PostgresSettings = PostgresSettings()
    pg_url = f"postgresql://{db.user}:{db.password}@{db.host}:{db.port}/{db.name}"

    LocalStorage = join(BASE_DIR, 'storage.json')
    templates = jinja2.FileSystemLoader(join(BASE_DIR, 'templates'))
    initial_state = datetime.min.isoformat()


settings = Settings()
