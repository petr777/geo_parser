from resources.subject import Subject
from models.geometry import GeometryPolygon
from shapely.geometry import Point
from resources.task import Task
from shapely import wkb
from loguru import logger
import numpy as np


def generator(subject_id, polygon, lat_step=0.022, lon_step=0.028):

    polygon = GeometryPolygon(polygon)
    buffer_shape = polygon.buffer_shape

    lats = polygon.get_all_lats()
    lons = polygon.get_all_lons()

    for lon in np.arange(min(lons), max(lons) + lon_step, lon_step):
        batch = list()
        for lat in np.arange(min(lats), max(lats) + lat_step, lat_step):
            point = Point((round(lat, 6), round(lon, 6)))
            if buffer_shape.contains(point):
                batch.append({
                    'subject_id': subject_id,
                    'point': wkb.dumps(point, hex=True)
                })
        yield batch


def generate_task(subject):
    if subject.type_geometry == 'Polygon':
        yield from generator(subject.id, subject.shape)
    if subject.type_geometry == 'MultiPolygon':
        for polygon in subject.shape.geoms:
            yield from generator(subject.id, polygon)


def strat(subject):
    task_service = Task()

    for batch in generate_task(subject):
        batch_task = task_service.add_many_task(
            batch, index_elements=['point']
        )
        log_str = f'{subject.name} - {batch_task}'
        logger.info(log_str)


if __name__ == '__main__':
    subject_service = Subject()
    subjects = subject_service.get_subject_by_country_name('Россия')

    for subject in subjects:
        strat(subject)
