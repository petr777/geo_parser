from core.constants import RADIUS, PAGE_SIZE, API_KEY, ALL_FIELDS



def params_maker(lat: float, lon: float, page: int = 1, **kwargs):

    params = {
        'key': API_KEY,
        'point': f'{lon},{lat}',
        'page': page,
        'page_size': PAGE_SIZE,
        'radius': RADIUS,
        'type': 'building',
        'fields': ','.join(ALL_FIELDS),
        'locale': 'ru_RU',
    }
    return params
