from resources.task import Task
from resources.getter import GetterGeometry
from urllib.parse import urlencode


task_service = Task()

batch_task = task_service.get_batch(subject_id='0980e7d4-6295-4853-8b1d-667f930679bd', limit_batch=10000)
getter_service = GetterGeometry()

# 41.311158, 69.279737
for task in batch_task:

    data = task.to_dict()
# 55.755864, 37.617698
    params = getter_service.params_maker(
        lat=data.get('lat'),
        lon=data.get('lon'),
        page=1
    )
    url = 'http://127.0.0.1:8000?' + urlencode(params)
    print(url)