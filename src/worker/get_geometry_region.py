from resources.country import Country
from resources.getter import GetterGeometry
from resources.subject import Subject


def get_params(**kwargs):
    fields = ['items.adm_div', 'items.geometry.centroid', 'items.geometry.selection']
    params = {
        'fa821dba_ipp_key': 'v1649510883228/v3394bd400b5e53a13cfc65163aeca6afa04ab2/8LZPTVBKtgDBsER0WxlAag==',
        'fa821dba_ipp_uid': '1649454460184/W3CPjKB74j4Dr4UE/ur15LzVKGPPf27BtZlf/KA==',
        'type': 'adm_div',
        'fields': ','.join(fields),
        'key': 'rurbbn3446',
        'locale': 'ru_RU',
    }
    if kwargs.get('q'):
        params['q'] = kwargs.get('q')
    return params


def start(country_name):

    country_service = Country()
    subject_service = Subject()
    getter_geometry = GetterGeometry()

    country = country_service.get_by_name(country_name)
    regions = country_service.get_region_names(country.name)

    for region in regions:
        params = get_params(q=region)
        getter_geometry.goto('https://catalog.api.2gis.ru/3.0/items?', params)

        data = getter_geometry.get_json()
        data = data['result']['items'][0]

        subject = {
            'name': data.get('full_name'),
            'geometry': data.get('geometry', {}).get('selection'),
            'country_id': country.id
        }

        subject_service.add(subject, index_elements=['name'])


if __name__ == '__main__':
    start('Узбекистан')
