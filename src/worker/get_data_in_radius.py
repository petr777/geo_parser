from resources.task import Task
from resources.getter import GetterGeometry
from services.params_maker import params_maker
from urllib.parse import urlencode

task = Task()


tasks = task.get_batch('0980e7d4-6295-4853-8b1d-667f930679bd')

task_params = [params_maker(**task.to_dict()) for task in tasks][:10]

getter_geometry = GetterGeometry()

for params in task_params:
    url = 'http://catalog.api.2gis.ru/2.0/geo/search?' + urlencode(params)
    print(url)
    # getter_geometry.goto('http://127.0.0.1:5000?', url)

# 41.279686, 69.182520