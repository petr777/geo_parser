

class GeometryPolygon:

    def __init__(self, polygon):
        self.polygon = polygon

    @property
    def buffer_shape(self):
        return self.polygon.buffer(0.05)

    def get_all_lats(self):
        return [x for x, y in self.polygon.exterior.coords[:-1]]

    def get_all_lons(self):
        return [y for x, y in self.polygon.exterior.coords[:-1]]
