import uuid
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Column, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry
from db.postgis import Base, engine
from geoalchemy2.shape import to_shape
from shapely.geometry import Point
from datetime import datetime


class CountryModel(Base):

    __tablename__ = 'country'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, unique=True)
    subjects = relationship(
        "SubjectModel",
        back_populates="country",
        cascade="all, delete, delete-orphan"
    )
    __table_args__ = {'schema': 'content'}

    def __repr__(self):
        return "<Country (name='%s')>" % self.name

    def to_dict(self):
        return {
            'id': str(self.id),
            'name':  self.name,
            'subjects': [sub.to_dict() for sub in self.subjects]
        }


class SubjectModel(Base):

    __tablename__ = "subject"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, unique=True)
    geometry = Column(Geometry("GEOMETRY", srid=4326))
    country_id = Column(UUID(as_uuid=True), ForeignKey('content.country.id'))
    country = relationship("CountryModel", back_populates="subjects")

    tasks = relationship(
        "TaskModel",
        back_populates="subject",
        cascade="all, delete, delete-orphan"
    )

    __table_args__ = {'schema': 'content'}

    def __repr__(self):
        return "<Country (name='%s', country='%s') >" % (
            self.name, self.country
        )

    def to_dict(self):
        return {
            'id': str(self.id),
            'name':  self.name,
            'geometry': self.shape.__geo_interface__
        }

    @property
    def shape(self):
        return to_shape(self.geometry)

    @property
    def type_geometry(self):
        return self.shape.type


class TaskModel(Base):

    __tablename__ = "task"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    point = Column(Geometry("POINT", srid=4326), unique=True)
    subject_id = Column(UUID(as_uuid=True), ForeignKey('content.subject.id'))
    subject = relationship("SubjectModel", back_populates="tasks")

    visited = Column(DateTime, default=datetime.now())

    __table_args__ = {'schema': 'content'}

    def __init__(self, point: Point, subject_id: str):
        self.point = point
        self.subject_id = subject_id

    @property
    def shape(self):
        return to_shape(self.point)

    def to_dict(self):
        return {
            'id': str(self.id),
            'lat': self.shape.coords[0][0],
            'lon': self.shape.coords[0][1]
        }

# Base.metadata.create_all(engine, checkfirst=True)