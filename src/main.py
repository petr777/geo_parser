import aiohttp_jinja2
from aiohttp import web
from pydantic import BaseModel
from core.config import settings


class RequestQuery(BaseModel):
    lon: float
    lat: float


@aiohttp_jinja2.template('index.html')
async def handle(request):
    query = RequestQuery(**request.rel_url.query)
    return query.dict()


app = web.Application()
aiohttp_jinja2.setup(app, loader=settings.templates)
app.router.add_get('/', handle)

web.run_app(app, port=5858)

